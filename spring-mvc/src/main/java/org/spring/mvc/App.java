package org.spring.mvc;

import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.HandlerExecutionChain;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	SimpleUrlHandlerMapping simpleHandlerMapping;
    	HandlerMapping handlerMapping;
    	HandlerExecutionChain chain;
    	XmlWebApplicationContext xmlContext;
    	WebApplicationContext context;
    	DispatcherServlet servlet;
    	ContextLoader contextLoader;
    	ContextLoaderListener listener;
    	
        System.out.println( "Hello World!" );
    }
}
