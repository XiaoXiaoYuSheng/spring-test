package org.spring.aop;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * Hello world!
 *
 */
@ComponentScan
@EnableAspectJAutoProxy
public class App 
{
	public static void main(String[] args) {
		System.out.println("--------------");
		ApplicationContext content = new AnnotationConfigApplicationContext(App.class);
		Demoin demoin = (Demoin)content.getBean("demoin");
		System.out.println(demoin.toString());
		demoin.test();
	}
}
