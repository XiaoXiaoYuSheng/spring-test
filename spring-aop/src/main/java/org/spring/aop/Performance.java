package org.spring.aop;

public interface Performance {

	public void perform();
}
