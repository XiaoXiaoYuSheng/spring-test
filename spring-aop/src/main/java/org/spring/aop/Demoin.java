package org.spring.aop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Demoin {

	@Autowired
	private Performance performance;
	
	public void test() {
		performance.perform();
	}
}
