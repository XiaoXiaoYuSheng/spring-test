package org.spring.aop;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class Audience {

	@Pointcut("execution(* org.spring.aop.Performance.perform())")
	public void perform() {}
	
	@Before("perform()")
	public void silenceCellPhones() {
		System.out.println("Silencing cell phones" );
	}
	
	@Before("perform()")
	public void takeSeats() {
		System.out.println("Taking seats");
	}
	
	@After("perform()") 
	public void applause(){
		System.out.println("CLAP CLAP CLAP");
	}
	
	@After("perform()")
	public void demandRefund() {
		System.out.println("Demanding a refund");
	}
}
