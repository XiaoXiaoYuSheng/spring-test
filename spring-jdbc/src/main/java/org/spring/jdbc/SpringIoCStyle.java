package org.spring.jdbc;

import java.sql.ResultSet;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class SpringIoCStyle {

	public static void main(String[] args) {
		// 创建并初始化IoC容器
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("config.xml");
		
		String sql = "select * from USER_TABLES";
		
		// 获取JdbcTemplate bean对象
		JdbcTemplate jdTemplate = (JdbcTemplate) applicationContext.getBean("jdbcTemplate");
		
		// 执行查询操作，并返回结果集，这个过程中会创建数据库连接、执行查询、解析结果、释放资源、返回结果集
		SqlRowSet rowSet = jdTemplate.queryForRowSet(sql);
		
		// 处理结果
		while(rowSet.next()) {
			System.out.println("TABLE_NAME: " + rowSet.getString("TABLE_NAME"));
		}
	}
}
