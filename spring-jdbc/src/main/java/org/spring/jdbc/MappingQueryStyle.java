package org.spring.jdbc;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MappingQueryStyle {

	public static void main(String[] args) {
		// 创建并初始化IoC容器
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("config.xml");
		
		DataSource dataSource = (DataSource)applicationContext.getBean("dataSource");
		TableMappingQuery tableMappingQuery = new TableMappingQuery(dataSource);
		
		List<Table> tableList = tableMappingQuery.execute();
		
		for (Table table : tableList) {
			System.out.println("TABLE_NAME: " + table.getTableName());
		}
	}
}
