package org.spring.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.MappingSqlQuery;

public class TableMappingQuery extends MappingSqlQuery {

	public TableMappingQuery(DataSource dataSource) {
		super(dataSource, "select * from USER_TABLES");
		//super.declareParameter(new SqlParameter(Types.INTEGER));
		compile();
	}
	
	@Override
	protected Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		Table table = new Table();
		table.setTableName(rs.getString("TABLE_NAME"));
		table.setTableSpaceName(rs.getString("TABLESPACE_NAME"));
		table.setClusterName(rs.getString("CLUSTER_NAME"));
		table.setLotName(rs.getString("IOT_NAME"));
		return table;
	}

}
