package org.spring.jdbc;

import lombok.Data;

@Data
public class Table {
	
	private String tableName;
	private String tableSpaceName;
	private String clusterName;
	private String lotName;
}
