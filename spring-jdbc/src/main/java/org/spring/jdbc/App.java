package org.spring.jdbc;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;

/**
 * Hello world!
 *
 */
public class App 
{
	JdbcTemplate jdbcTemplate;
	DataSourceUtils dataSourceUtils;
	
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
}
