package org.spring.Ioc.soundsystem;

import org.springframework.stereotype.Component;
import javax.inject.Named;

//@Component("mediaPlayer")
@Component("lonelyHeartsClub")
//@Named("lonelyHeartsClub")
public class SgtPeppers implements CompactDisc {

	private String title = "Sgt. Pepper's Lonely Hears Club Band";
	private String artist = "The Beatles";
	
	public void play() {
		System.out.println("Playing " + title + " by " + artist);
	}
	
	@Override
	public void playTrack(int trackNumber) {
		// TODO Auto-generated method stub
		System.out.println("Playing " + title + " by " + artist);
	}

	public void test() {
		// TODO Auto-generated method stub
		
	}

}
