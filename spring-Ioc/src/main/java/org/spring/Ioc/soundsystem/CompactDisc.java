package org.spring.Ioc.soundsystem;

public interface CompactDisc {
	void play();
	void playTrack(int trackNumber);
	void test();
}
