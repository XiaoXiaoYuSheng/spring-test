package org.spring.Ioc;

import java.io.PrintStream;

public class SlayDragonQuest implements Quest{

	private PrintStream stream;
	
	public SlayDragonQuest(PrintStream stream) {
		this.stream = stream;
	}
	
	public void embark() {
		// TODO Auto-generated method stub
		stream.print("Embraking on quest to slay the dragon!");
	}

}
