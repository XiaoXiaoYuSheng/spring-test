package org.spring.Ioc;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App {
	public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        
        String projectPath = System.getProperty("user.dir");
        // 注knight.xml位于target目录下
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("knights.xml");
        //ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("file:E:\\java_workspace\\SpringTestProjects\\spring-test\\src\\resource\\knights.xml");
        //BraveKnight knight = context.getBean(BraveKnight.class);
        //Knight knight = (Knight)context.getBean("knight");
        Knight knight = context.getBean(Knight.class);
        knight.embarkOnQuest();
        context.close();
    }
}
